const Group = require('../models/group.model');

const groupGet = async (req, res, next) =>{
    try{
        const groups = await Group.find();
        //  console.log(groups);
        return res.status(200).json(groups);
    }catch(err){
        const myError = new Error("Los grupos no se han encontrado");
        return next(myError);
    }
}

const groupPost = async (req, res, next) =>{
       const imageUrl = req.file_url;
    try{
        const { name, style} =  req.body;
        const newGroup = new Group({name, style, image: imageUrl });
        const createGroup = await newGroup.save();
        return res.status(200).json(createGroup);
    } catch(err){
        const myError = new Error("El grupo no se ha conseguido crear");
        return next(myError);
    }
}

const groupPut =  async (req, res, next) => {
    try{
        const {id, name, style} =  req.body;
        const update = {};
        if(name) update.name = name;          
        if(style) update.style = style;        
         
        const updateGroup = await Group.findByIdAndUpdate(
            id, update,{new : true});
            return res.status(200).json(updateGroup);    
    } catch(err){
        const myError = new Error("El grupo no se ha conseguido editar");
        return next(myError);
    }
}

const groupDelete = async (req,res,next) =>{
    const { id } = req.params;
    try{
         await Group.findByIdAndDelete(id);
         
        return res.status(200).json("Has eliminado el grupo");
        
    }catch(err){
        const myError = new Error("El grupo que buscas no ha sido eliminado");
        return next(myError);
    }
    
}

module.exports = {groupGet, groupDelete, groupPut, groupPost}