const Festival = require('../models/festival.model');

const festivalGet = async (req, res, next) => {
    try{
        const festivals = await Festival.find().populate("groups");
        console.log(festivals);
        return res.status(200).json(festivals);

    } catch(err){
        const myError = new Error("Los festivales no se ha encontrado");
        return next(myError);
    }
}

const festivalPost = async (req, res, next) => {
    try{
        const {name, location} = req.body;
        const newFestival = new Festival({name, location});
        const createFestival = await newFestival.save();
        console.log(createFestival);
        return res.status(200).json(createFestival);
    }catch(err){
        const myError = new Error("El festival no se ha conseguido crear");
        return next(myError);
    }
}

const festivalPut =  async (req, res, next) => {
    try {
      const festivalId = req.body.festivalId;
      const groupId = req.body.groupId;

      if(!festivalId || !groupId){
          const error = new error("faltan argumentos");
          error.status(400);
          throw error;
      }
  
      const updatedFestival = await Festival.findByIdAndUpdate(
        festivalId,
        { $addToSet: { groups: groupId } },
        { new: true }
      );
  
      return res.status(200).json(updatedFestival);
    } catch (err) {
        return next(err);
      
    }
  }


module.exports = {festivalGet, festivalPost, festivalPut, }