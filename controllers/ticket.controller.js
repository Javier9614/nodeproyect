const Ticket = require("../models/ticket.model");
const util = require("../utils/utilsTicket");
const { eventPutCapacity } = require("../controllers/event.controller");
const {insertTicket} = require("../controllers/user.controler");

const ticketGet = async (req, res, next) => {
    try{
        const tickets = await Ticket.find().populate({
            path:"event",
            populate: {path: "eventIdConcert"},
        }).populate({
            path:"event",
            populate: {path: "eventIdFestival"},
        });
        return res.status(200).json(tickets);
    }catch(err){
        const myError = new Error("Los eventos no se ha encontrado");
        return next(myError);
    }
    
}

  const ticketPost =  async (req, res, next) => {
    try {
      const { event } = req.body;
      
      const capEvent = await util.capacityEvent(event);
      
  
      if (capEvent === 0) {
        return res.json("Sold Out");
      };
  
      const newTicket = new Ticket({ event });
      
      const newEventCapacity = await eventPutCapacity(event,1);
      
      if (newEventCapacity === null) {
          throw error;
      };
      const createTicket = await newTicket.save();
    
      const addInUser= await insertTicket(req.user.id, createTicket.id);
     
      if(addInUser === null){
        return new Error("No funciona");
      }
      
     const change = await changeActive(createTicket.id)
     change;
      return res.status(200).json(createTicket);
    } catch (error) {
      const myError = new Error("El Ticket no se ha conseguido crear");
      return next(myError);
    }
  }

  const changeActive = async (id) =>{
    
    try {
       const prueba =await Ticket.findByIdAndUpdate (
        id, 
        {$set: {isActive: true}},
        {new: true}
      );
      return prueba;

      } catch (error) {
        return null;
      }
  }


  

  module.exports = {ticketGet, ticketPost};