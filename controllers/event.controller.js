const Event = require("../models/event.model");
const utils = require("../utils/utilsEvent");


const eventGet = async (req, res, next) => {
    try{
        const events = await Event.find().populate(
            {
                path:"eventIdConcert",
                populate:{path:"group"}

        }).populate(
            {
                path:"eventIdFestival",
                populate:{path:"groups"}

        });
        
        return res.status(200).json(events);
    }catch(err){
        const myError = new Error("Los eventos no se ha encontrado");
        return next(myError);
    }
}

const eventConcertGet = async(req, res, next) => {
    try{
        const eventsConcert = await Event.find({ eventType: 'Concert'}).populate({
            path:"eventIdConcert",
            populate: {path: "group"}
        });
        return res.status(200).json(eventsConcert);
    }catch(err){
        const myError = new Error("Los eventos no se ha encontrado");
        return next(myError);
    }
    
}
const eventFestivalGet = async(req, res, next) => {
    try{
        const eventsFestival = await Event.find(({ eventType: 'Festival'})).populate({
            path:"eventIdFestival",
            populate: {path: "groups"}
        });
        return res.status(200).json(eventsFestival);
    }catch(err){
        const myError = new Error("Los eventos no se ha encontrado");
        return next(myError);
    }
    
}

const eventCreate =  async(req, res, next) => {
    try {
        const {date ,startTime,eventType,eventIdFestival,eventIdConcert,capacity,price} = req.body;
        const newEvent = new Event({date ,startTime,eventType,eventIdFestival,eventIdConcert,capacity,price});
        if(newEvent.eventType === "Concert" && newEvent.eventIdConcert !== null){
            //aqui comprobamos que el evento sea de tipo concierto
                //aqui comprobaremos que la id sea distinta a null
                const eventConcert = await utils.findConcert(newEvent.eventIdConcert);

                if(eventConcert !== null){
                     //aqui comprobamos que la id sea igual a una de concierto, y si no lo es no entrara aqui
                    newEvent.capacity = 100;
                    newEvent.price = 13;
                    newEvent.eventIdConcert = eventConcert;
                    console.log(newEvent);
                    const createConcertEvent = await newEvent.save();
                   // console.log("eventfestival", createConcertEvent );
                    return res.status(200).json(createConcertEvent);
                }
            
            const error = new Error("El id del concert es null");
            return res.json(error.message);
        }else{
            //entro aqui si es un festival
            // compruebo si el id es null
            //console.log('eventid',newEvent.eventId);
            if(newEvent.eventIdFestival !== null){
                 // console.log("es festival y el id es distinto de nulo");
                  const eventFestival = await utils.findFestival(newEvent.eventIdFestival);
                  
                  if(eventFestival !== null){
                      //console.log(eventFestival);
                      newEvent.capacity = 1000;
                      newEvent.price = 60;
                      newEvent.eventIdFestival = eventFestival;
                      console.log(newEvent);
                      const createFestivalEvent = await newEvent.save();
                      console.log("eventfestival", createFestivalEvent );
                    return res.status(200).json(createFestivalEvent);
                  }
                  return res.json('festival');

            }
            const error = new Error("El id del fest es null");
            return res.json(error.message);   
        }
     
    } catch (error) {
        const myError = new Error("El evento no se ha conseguido crear");
        return next(myError);
    }
}



const eventPutCapacity =  async (id, rest) => {

    try {
        const concert = await Event.findById(id);
        const update={}
        update.capacity = (concert.capacity - rest)
        return await Event.findByIdAndUpdate(
            id, 
            update,
            {new: true}
    );    
    } catch (error) {
        return null;
    }
}



module.exports = {eventCreate, eventConcertGet, eventFestivalGet, eventGet, eventPutCapacity}


