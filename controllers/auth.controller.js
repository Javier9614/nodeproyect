const passport = require('passport');

const registerGet = (req, res, next) => {

    return res.json("ven a registrarte");
    //return res.render("./auth/register");
}

const registerPost = async (req, res, next) =>{
    //recibe el parametro de la estrategia creada en el index de auth, en este caso register
    const done = ( error, user) =>{
        if(error){
            return next(error)
        }
        console.log("Usuario registrado -->", user);
        return res.json(user);//CHANGE BEFORE WORKS FINE
    }
    passport.authenticate("register",done)(req);
}

const loginGet = (req, res, next) =>{
    return res.json("Estas registrado?");
}

const loginPost = (req, res, next) =>{
    const done = ( error, user) =>{
        if(error){
            return next(error);
        }
        req.logIn(user, (error)=>{

            if(error){
                return next(error);
            }
            console.log("Usuario logueado", user);
            return res.json(user);//CHANGE BEFORE WORKS FINE
        })
        
    };
    passport.authenticate("access", done)(req);
}

const logoutPost = (req, res, next) =>{
    console.log("dentro de logout");
    if(req.user){
    req.logout();
    req.session.destroy(() =>{
        res.clearCookie("connect.sid");
        return res.redirect("/");
    });
}
}
module.exports = {registerGet, registerPost, loginGet, loginPost, logoutPost };