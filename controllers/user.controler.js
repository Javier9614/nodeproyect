const User = require ("../models/user.model");

const  insertTicket = async (id,ticketId) => {
    try{
    return await User.findByIdAndUpdate(

        id, 
        {$addToSet: {tickets: ticketId}},
        {new: true}

    );
    }catch(err){
    return null
}
}


module.exports = {insertTicket};