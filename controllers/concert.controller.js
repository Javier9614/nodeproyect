const Concert = require('../models/concerthall.model');

const concertGet = async (req, res, next) => {
    try{
        const concerts = await Concert.find().populate("group");
        console.log(concerts);
        return res.status(200).json(concerts);

    } catch(err){
        const myError = new Error("Los conciertos no se ha encontrado");
        return next(myError);
    }
}

const concertPost = async (req, res, next) => {
    try{
        const {name, locality,street} = req.body;
        const newConcert = new Concert({name, locality,street});
        const createConcert = await newConcert.save();
        console.log(createConcert);
        return res.status(200).json(createConcert);
    }catch(err){
        const myError = new Error("El Concert no se ha conseguido crear");
        return next(myError);
    }
}

const concertPut = async (req, res, next) => {
    try {
      const concertId = req.body.concertId;
      const groupId = req.body.groupId;

      if(!concertId || !groupId){
          const error = new error("faltan argumentos");
          error.status(400);
          throw error;
      }
  
      const updatedConcert = await Concert.findByIdAndUpdate(
        concertId,
        { $push: { group: groupId } },
        { new: true }
      );
  console.log(updatedConcert);
      return res.status(200).json(updatedConcert);
    } catch (err) {
        return next(err);
      
    }
  }

  module.exports = {concertGet, concertPost, concertPut, }