const mongoose = require('mongoose');
const Concert = require('../models/concerthall.model');
const db = require('../config/db');



const concerts = [
    {
        name:"Sala Lopez" ,
        locality:"Zaragoza",
        street:"Calle de Sixto Celorrio, 2",
        
        
    },
    {
        name:"La Casa Del Loco" ,
        locality:"Zaragoza",
        street:"Calle Mayor, 10",
        
        
    },
    {
        name:"Las Armas" ,
        locality:"Zaragoza",
        street:"Plaza Mariano de Cavia, 2",
        
        
    },
   
];

mongoose
    .connect(db.DB_URL, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(async () => {
        const allConcerts = await Concert.find();
        
    
        if (allConcerts.length){
         //si la coleccion existe la eliminamos 
         
            console.log(`Encontradas ${allConcerts.length} salas`);   
            await Concert.collection.drop();        
            console.log("Coleccion eliminada correctamente");

        } else {
            console.log("No se encontraron Salas")
        }
    })

    .catch(error => console.log("error eliminando la coleccion", error))
    .then(async() => {
        //despues de eliminar la coleccion voy a crear los grupos iniciales en la base de datos

        await Concert.insertMany(concerts);
        console.log("OK: Salas creadas correctamente")
    })

    .catch(error => console.log("error añadiendo las salas"))
    //sirve para desconectarnos de la base de datos 
    .finally(() => mongoose.disconnect());