const mongoose = require('mongoose');
const Group = require('../models/group.model');
const db = require('../config/db');



const groups = [
    {
        name:"Avenged Sevendfold" ,
        style: "Metalcore",
        image:"https://res.cloudinary.com/ds55sjiq9/image/upload/v1628791824/a7x-logo_fa0cz8.jpg"
    },
    {
        name:"Slipknot" ,
        style: "Nu metal",
        image:"https://res.cloudinary.com/ds55sjiq9/image/upload/v1628791815/slipknot_gqerag.jpg"
    },
    {
        name:"Binary Mood" ,
        style: "Trap",
        image:"https://res.cloudinary.com/ds55sjiq9/image/upload/v1628791862/binary-mood_hl0g25.jpg"
    },
    {
        name:"Bullet For My Valentine" ,
        style: "Metalcore",
        image:"https://res.cloudinary.com/ds55sjiq9/image/upload/v1628791878/bullet-for-my-valentine_kxromw.jpg"
    },
    {
        name:"Natos y Waor" ,
        style: "Rap",
        image:"https://res.cloudinary.com/ds55sjiq9/image/upload/v1628791805/natos-y-waor_c90dks.png"
    },
    {
        name:"Dj Tiesto" ,
        style: "Electrónica",
        image:"https://res.cloudinary.com/ds55sjiq9/image/upload/v1628791796/dj-tiesto_cz22ur.jpg"
    },
];

//conectamos la seed a la base de datos
mongoose
    .connect(db.DB_URL, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(async () => {
        const allGroups = await Group.find();
        
    
        if (allGroups.length){
         //si la coleccion existe la eliminamos 
         
            console.log(`Encontrados ${allGroups.length} grupos`);   
            await Group.collection.drop();        
            console.log("Coleccion eliminada correctamente");

        } else {
            console.log("No se encontraron grupos")
        }
    })

    .catch(error => console.log("error eliminando la coleccion", error))
    .then(async() => {
        //despues de eliminar la coleccion voy a crear los grupos iniciales en la base de datos

        await Group.insertMany(groups);
        console.log("OK: Grupos creados correctamente")
    })

    .catch(error => console.log("error añadiendo los grupos"))
    //sirve para desconectarnos de la base de datos 
    .finally(() => mongoose.disconnect());