const mongoose = require('mongoose');
const Festival = require('../models/festival.model');
const db = require('../config/db');




const festivals = [
    {
        name:"Resurrection Fest" ,
        location:"Viveiro, Viveiro, España",
        group:[]
    },
    {
        name:"Viña Rock" ,
        location:"Viveiro, Viveiro, España",
        group:[]
    },
    {
        name:"Arenal Sound" ,
        location:"Viveiro, Viveiro, España",
        group:[]
    },
   
];

mongoose
    .connect(db.DB_URL, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(async () => {
        const allFestivals = await Festival.find();
        
    
        if (allFestivals.length){
         //si la coleccion existe la eliminamos 
         
            console.log(`Encontradas ${allFestivals.length} festivales`);   
            await Festival.collection.drop();        
            console.log("Coleccion eliminada correctamente");

        } else {
            console.log("No se encontraron festivales")
        }
    })

    .catch(error => console.log("error eliminando la coleccion", error))
    .then(async() => {
        //despues de eliminar la coleccion voy a crear los grupos iniciales en la base de datos

        await Festival.insertMany(festivals);
        console.log("OK: festivales creados correctamente")
    })

    .catch(error => console.log("error añadiendo los festivales"))
    //sirve para desconectarnos de la base de datos 
    .finally(() => mongoose.disconnect());