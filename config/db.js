//Requerimos mongoose
const  mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

//creamos una constante para guardar la url de la base de datos de mongo y automaticamente creara una base de datos si no existe
const DB_URL = process.env.DB_URL || "mongodb://localhost:27017/music-entry";

// ponemos el try catch en una funcion con async await para que conecte a mongo desde node
const connect = async () => {
    try{
        await mongoose.connect(DB_URL, {
            //   ponemos useNewUrlParser y useUnifiedTopology, y las iniciamos en true
            useNewUrlParser:true,
            useUnifiedTopology:true,
            useFindAndModify:false
        });

        console.log("Connect to Data Base");
    }catch (error) {
        console.log(`Ha ocurrido un error conectando a la base de datos ${error}`);
    };
}



module.exports = {DB_URL, connect};


