//requerimos todos los paquetes necesarios en index
const express = require('express');
const cors = require('cors');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const passport = require('passport');
const dotenv = require('dotenv');

dotenv.config();


const auth = require('./auth');


auth.setStrategies();


//requerimos que conecte a los routes
const groupRoutes = require('./routes/Group.routes');
const indexRoutes = require('./routes/Index.routes');
const festivalRoutes = require('./routes/Festival.routes');
const concertRoutes = require('./routes/Concert.routes');
const eventRoutes = require('./routes/Event.routes');
const ticketRoutes = require('./routes/Ticket.routes');
const authRoutes = require('./routes/Auth.routes');

//requerimos que conecte al archivo db.js y asi nos conectamos a la base de datos
const db = require("./config/db");
db.connect();

//Definimos el puerto en el que iniciara node
const PORT = process.env.PORT || 4000;

let corsOptions = {
    origin: 'http://localhost:${PORT}'
}

const app = express();

app.use(cors(corsOptions));

app.use(session({
    secret:process.env.SESSION_SECRET ||"#$%^hjkdHJD54767*",
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge:24 * 60 * 60 * 1000
        
    },
    store: MongoStore.create({mongoUrl: db.DB_URL}) ,
}));
//Configuramos a express para que use el paquete passport para autentificacion.
app.use(passport.initialize());

//Añadira nuestro usuario al objeto req.user, que si hay un usuario y sse loguea cojera al usario y lo metera en la req
//por lo que tendremos al usuario disponible en cualquier endpoint
app.use(passport.session());

//Reconoce la peticion que entra y si tiene algo en el body lo parsea y nos lo envia al req.body
app.use(express.json());
//Interpresta un formulario y lo añade a la base de datos
app.use(express.urlencoded({ extended: true }));

app.use("/", indexRoutes);
app.use("/groups", groupRoutes);
app.use("/festivals", festivalRoutes);
app.use("/concerts", concertRoutes);
app.use("/events", eventRoutes);
app.use("/tickets", ticketRoutes);
app.use("/auth", authRoutes);

app.use("*" ,(req, res, next) => {
    const error = new Error("La página que buscas no existe");

    return res.status(404).json(error.message);
})

app.use((error, req, res, next) => {

    console.log(error);
    return res.status(error.status || 500).json(error.message || "Unexpected Error");
})

app.listen(PORT, () =>{
    console.log(`El servidor esta corriendo en http://localhost:${PORT}`);
});

