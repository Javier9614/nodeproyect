const express = require('express');
const controller = require("../controllers/festival.controler");
const router = express.Router();
const {isAuth , isAdmin} = require('../middleware/auth.middlewares');

router.get('/',[isAuth], controller.festivalGet )

router.post('/create',[isAuth],[isAdmin], controller.festivalPost)

router.put('/add-group',[isAuth],[isAdmin], controller.festivalPut);


module.exports = router;