const express = require('express');

const controller = require("../controllers/group.controller");

const {upload, uploadToCloudinary} = require ("../middleware/file.middleware")

const router = express.Router();

const {isAuth , isAdmin} = require('../middleware/auth.middlewares');

router.get("/", [isAuth],[isAdmin], controller.groupGet);

router.post("/create",[isAdmin],[upload.single('image'), uploadToCloudinary], controller.groupPost);

router.put("/edit",[isAdmin], controller.groupPut);

router.delete("/:id",[isAdmin], controller.groupDelete);


module.exports = router;

