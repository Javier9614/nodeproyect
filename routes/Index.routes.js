const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
    const msg = "Hasta pronto";
    console.log(msg);
    return res.send(msg);
});

router.get('/vecino', (req, res) => {
    return res.send('yeeeee');
});

router.get('/prueba-error', (req,res,next) => {
    const error = new Error("La lie");
    next(error);
})

module.exports = router;