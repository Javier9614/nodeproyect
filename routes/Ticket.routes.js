const express = require("express");

const router = express.Router();

const controller = require("../controllers/ticket.controller");

router.get("/", controller.ticketGet);

router.post("/create", controller.ticketPost);


module.exports = router;
