const express = require("express");

const controller = require("../controllers/event.controller");

const router = express.Router();

const { isAuth, isAdmin } = require("../middleware/auth.middlewares");

router.get("/", controller.eventGet);

router.get("/concerts", controller.eventConcertGet);

router.get("/festivals", controller.eventFestivalGet);

router.post("/create", [isAuth], [isAdmin], controller.eventCreate);

   

module.exports = router;
