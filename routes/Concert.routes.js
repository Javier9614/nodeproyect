const express = require('express');
const controller = require("../controllers/concert.controller");
const router = express.Router();
const {isAuth , isAdmin} = require('../middleware/auth.middlewares');

router.get('/',[isAuth], controller.concertGet)

router.post('/create',[isAuth],[isAdmin], controller.concertPost )

router.put('/add-group',[isAuth],[isAdmin], controller.concertPut );


module.exports = router;