const isAuth = (req, res, next) => {
    if(req.isAuthenticated()){
    return next()
    }
    console.log("Tienes que loguearte");
    return res.redirect("/auth/register");
};

const isAdmin = (req, res, next) => {
    /**
   * 1. Comprobar si el usuario está autenticado. Si no lo está redirigimos a login.
   * 2. Comprobar que la propiedad user.role === 'admin'.
   * Si está autenticado pero no es admin, redirigimos a /pets
   */
if(req.isAuthenticated()){
    if(req.user.role==="admin"){
        return next();
    }
    console.log("no eres admin")
    return res.redirect("/groups");
}
return res.redirect("/auth/register");
}

module.exports = {
    isAuth,
    isAdmin,
}