 const LocalStrategy = require("passport-local").Strategy;

 const bcrypt = require('bcrypt');

 const User = require ("../models/user.model");

 const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validatePass = (password) => {
    const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    return re.test(String(password));
}


 const registerStrategy = new LocalStrategy(
     {
            //usernameField hace referencia al valor del input
         usernameField:"email",
         passwordField:"password",
         passReqToCallback: true

     },
     
     async(req, email, password, done) => {

        try{
            const existingUser = await User.findOne({ email });

            if(existingUser){
                //no crear user
                const error = new Error("Usuario ya registrado");
                return done(error);
            }

            const isValidEmail = validateEmail(email);

            if(!isValidEmail){
                const error = new Error("Email incorrecto. No cumple el formato");
                return done(error);

            }

            const isValidPassword = validatePass(password);

            if(!isValidPassword){
                const error = new Error("Contraseña incorrecta. Tiene que contener minimo 6 carácteres, una minúscula, una mayúscula y un número");
                return done(error);
            }
            //el numero de veces que encriptaremos la contraseña
            const saltRounds = 10;
            const hash = await bcrypt.hash(password,saltRounds);

            const newUser = new User({
                email,
                password: hash,
                name:req.body.name
            });

            const savedUser = await newUser.save();
            savedUser.password = undefined; //IMPORTANTE!!!!!!!!!!!!

            return done(null, savedUser);
        }catch(error){
            return done(error);
        }

     });

 module.exports = registerStrategy;