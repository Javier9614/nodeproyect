const mongoose = require('mongoose');

const{ Schema } = mongoose;

const ticketSchema = new Schema(
    {
        event:{type:mongoose.Types.ObjectId, ref:"Events"},
        timeGetted:{ type : Date, default: Date.now },
        isActive:{ type : Boolean, default:false},

    },
        {timestamps: true}
    );

const Ticket = mongoose.model("Tickets", ticketSchema);

module.exports = Ticket;
