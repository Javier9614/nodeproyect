const mongoose = require('mongoose');

const {Schema} = mongoose;

const eventSchema = new Schema(
    {
        date:{type:String, required:true},
        startTime:{type:String, required:true},
        eventType:{type:String,enum:["Concert","Festival"],default:"Concert"},
        eventIdConcert:{type:mongoose.Types.ObjectId, ref:"Concert", default:undefined},
        eventIdFestival:{type:mongoose.Types.ObjectId, ref:"Festivals",default:undefined },
        capacity:{type: Number ,default:null},
        price: {type:Number, default:null}

    },
    {timestamps: true}
);

const Event = new mongoose.model("Events", eventSchema);

module.exports = Event;