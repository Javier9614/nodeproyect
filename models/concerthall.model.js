const mongoose = require('mongoose');

const { Schema } = mongoose;

const concertSchema = new Schema(
    { 
        name:{ type:String, required:true},
        locality:{ type:String, required:true},
        street:{ type:String, required:true},
        group:{type: mongoose.Types.ObjectId, ref: 'Groups'},
    },
    {timestamps: true}
    );

    const Concert = new mongoose.model("Concert", concertSchema);
    
    module.exports = Concert;