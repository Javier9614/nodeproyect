//se requerira mongoose en todos los archivos que se necesiten
const mongoose = require('mongoose');

//const Schema = mongoose.Schema;

const { Schema }= mongoose;

const  groupSchema = new Schema(
    {
        name: {type: String, required: true},
        style: {type: String, required: true},
        image:{type: String}
        
    },
    //es una funcion de configuracion que hara que se creen dos campos,
    //el momento en el que se crea y el momento de su ultima actualizacion
    {timestamps: true} 
);

const Group = mongoose.model("Groups", groupSchema);



module.exports = Group;