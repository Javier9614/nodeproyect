const mongoose = require('mongoose');

const  { Schema } = mongoose;

const festivalSchema = new Schema(
    {
        name:{type: String, required: true},
        location:{type: String, required: true},
        groups: [{type: mongoose.Types.ObjectId, ref: 'Groups' }]

    ,},
    {timestamps: true}
    );

    const Festival = mongoose.model("Festivals", festivalSchema);

    module.exports = Festival;