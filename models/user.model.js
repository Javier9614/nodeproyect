const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema(
    { 
        email:{type: 'string', required: true},
        password:{type: 'string', required: true},
        name:{type: 'string', required: true},
        role:{type: 'string', required: true, default: "user", enum: ['user', 'admin'] },
        tickets:[{type:mongoose.Types.ObjectId, ref:"Tickets"}]
},
    {timestamps: true}
);

const User = mongoose.model("User", userSchema);

module.exports = User;