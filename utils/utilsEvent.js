const Concert = require("../models/concerthall.model");
const Festival = require("../models/festival.model");

const findConcert = async(eventId) => {
    //console.log(eventId);
    try{
    const concert = await Concert.findById(eventId);
    //console.log("Funcion findconcert",concert)
    if(concert !==null){
        return concert;

    }
    return null;
}catch(err){
    return null
}
}

const findFestival = async(eventId) => {
    console.log('buscando el id',eventId);
    try{
    const festival = await Festival.findById(eventId);
    if(festival !== null){
        return festival;

    }
    return null;
}catch(err){
    return null
}
}

module.exports = {findConcert, findFestival}