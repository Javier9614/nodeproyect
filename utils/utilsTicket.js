const Event = require("../models/event.model");

const capacityEvent = async (id) => {
    try {
      const eventId = await Event.findById(id);
      maxCapacity = eventId.capacity;
      return maxCapacity;
    } catch (error) {
      return null;
    }
  };

  module.exports = {capacityEvent};